package main

import "fmt"

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {

	//alex := person{firstName: "alex", lastName: "perez"}

	/*alex := person{
		firstName: "alex",
		lastName: "perez",
		contact: contactInfo{
			email: "hola@gmail.com",
			zipCode: 23,
		}
	}*/

	//-----------------------------------
	var contact contactInfo
	contact.email = "hola@gmail.com"
	contact.zipCode = 23

	var alex person
	alex.firstName = "alex"
	alex.lastName = "perez"
	alex.contactInfo = contact

	alex.print()

	//aca obtenod la direccion en memoria de alex
	//alexPointer := &alex
	alex.updateName("pene")
	alex.print()
}

//estoy indicando que va a ser usado la funcion en un putero de person
func (pointerPerson *person) updateName(newFirstName string) {
	//obtengo el valor de la posicion de memoria
	(*pointerPerson).firstName = newFirstName
}

func (p person) print() {
	fmt.Printf("%+v", p)
}
