package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {

	if len(os.Args) >= 2 {
		content, err := os.Open(os.Args[1])
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}
		io.Copy(os.Stdout, content)

	} else {
		fmt.Println("Not found file argument")
		os.Exit(1)
	}
}
