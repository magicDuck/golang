package main

import "fmt"

type bot interface {
	getgreetin() string
}

type englishBot struct{}
type spanishBot struct{}

func main() {

}

func printGreeting(b bot) {
	fmt.Println(b.getgreetin())
}

func (eb englishBot) getgreetin() string {
	return "hello"
}

func (sb spanishBot) getgreetin() string {
	return "hola"
}
