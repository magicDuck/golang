package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

type deck []string

func newDeck() deck {
	cards := deck{}

	cardSuit := []string{"Spades", "Diamons", "Heats", "Clubs"}
	cardValues := []string{"ace", "two", "three", "four"}

	for _, suit := range cardSuit {
		for _, values := range cardValues {
			cards = append(cards, values+" of "+suit)
		}
	}

	return cards
}

func (cards deck) print() {
	for i, card := range cards {
		fmt.Println(i, card)
	}
}

func deal(cards deck, handSize int) (deck, deck) {
	return cards[:handSize], cards[handSize:]
}

func (cards deck) toString() string {
	return strings.Join([]string(cards), ",")
}

func (cards deck) saveToFile(fileName string) error {
	return ioutil.WriteFile(fileName, []byte(cards.toString()), 066)
}

func readToFile(fileName string) deck {

	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	s := strings.Split(string(content), ",")
	return deck(s)
}

func (d deck) shuffle() {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}
