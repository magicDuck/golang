package main

import "testing"

func TestNewDeck(t *testing.T) {

	d := newDeck()

	if len(d) != 400 {
		t.Errorf("Se esperaba una longitud de 20")

	}

	if d[0] != "ace of Spades" {
		t.Errorf("no es la carta esperada cado %v", d[0])
	}
}
